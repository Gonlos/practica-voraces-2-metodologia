/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicavoraces;

import java.util.LinkedList;

/**
 *
 * @author Juan Manuel
 */
public class Reparto {
    // declaramos las variables de la clase
    private double[] _distanciaEntreGasolineras = null;
    private double _numKmAutonomia;
    
    public Reparto(double[] distanciaEntreGasolineras, double numKmAutonomia) {
        _distanciaEntreGasolineras = distanciaEntreGasolineras;
        _numKmAutonomia = numKmAutonomia;
    }
    
    public double[] getDistanciaEntreGasolineras() {
        return _distanciaEntreGasolineras;
    }
    
    public LinkedList<Integer> hayarTrayectoExpress() {
        double distanciaAutonomiaActual = _numKmAutonomia;
        LinkedList<Integer> solucion = new LinkedList<Integer>();
        int indxCand = 0;
        double candidato = 0;
        int lCand = _distanciaEntreGasolineras.length;
        
        double distanciaRestante = calcularDistanciaTotal(_distanciaEntreGasolineras);
        
        // comprobamos que el conjunto no este vacio y que no sea una solucion
        while ((indxCand < lCand) && (!esSolucion(distanciaRestante, distanciaAutonomiaActual))) {
            // elegimos un candidato
            candidato = seleccionarCandidato(indxCand, _distanciaEntreGasolineras);
            
            // comprobamos si el candidato es parte de nuestra solución
            if (esFactible(distanciaAutonomiaActual, candidato)) {
                solucion.add(indxCand); // añadimos al candidato
                distanciaAutonomiaActual = _numKmAutonomia; // reseteamos los kilometros que podemos hacer
            }
            
            distanciaAutonomiaActual -= candidato; // le quitamos los kilometros realizados
            distanciaRestante -= candidato; // y a la distancia restante
            
            // lo quitamos del conjunto
            indxCand++;
        }
        
        return (esSolucion(distanciaRestante, distanciaAutonomiaActual)) ? solucion : null;
    }

    
    private double calcularDistanciaTotal(double[] arr) {
        int l = arr.length;
        double distancia = 0;
        for (int i = 0; i < l; i++) distancia += arr[i];
        
        return distancia;
    }
    
    private boolean esSolucion(double distanciaRestante, double distanciaAutonomiaActual) {
        return (distanciaRestante <= 0.000001) && // error en las operaciones
               (distanciaAutonomiaActual >= -0.000001);
    }
    
    private double seleccionarCandidato(int indxCandidato, double[] candidatos) {
        return candidatos[indxCandidato];
    }
    
    private boolean esFactible(double numKmAutonomiaRestante, double distanciaARecorrer) {
        return (numKmAutonomiaRestante < distanciaARecorrer - 0.000001) && // error
               (numKmAutonomiaRestante >= -0.000001);
    }    
}
