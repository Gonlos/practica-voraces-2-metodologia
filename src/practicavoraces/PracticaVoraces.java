/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicavoraces;

import java.util.*;

/**
 *
 * @author Juan Manuel
 */
public class PracticaVoraces {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner teclado = new Scanner(System.in);
        Reparto reparto = null;
        boolean salir;
        LinkedList<Integer> indicesGasolinerasParar = null;
        
        System.out.print("Programa para calcular las paradas necesarias para el servicio\n" + 
                         "express de mensajeria.\n" + 
                         "Desea introducir las gasolineras manualmente(m) o automaticamente(a).\n");
        
        salir = false;
        while (!salir) {
            System.out.print("Opcion: ");
            switch (teclado.next().charAt(0) ) {
                case 'm':
                case 'M':
                    reparto = insercionManual();
                    salir = true;
                    break;
                    
                case 'a':
                case 'A':
                    reparto = insercionAutomatica();
                    salir = true;
                    break;
                    
                default:
                    System.out.println("La opcion elegida no es valida.");
            }
        }
        
        double[] gasol = reparto.getDistanciaEntreGasolineras();
        indicesGasolinerasParar = reparto.hayarTrayectoExpress();
        
        if (indicesGasolinerasParar == null) {
            System.out.print("No es posible realizar el trayecto con los datos suministrados.");
            return ;
        }
        
        String s = "Las gasolineras en las que hay que parar son:";
        
        Integer indxAnt = 0;
        double distancia;
        for (Integer indx : indicesGasolinerasParar) {
            distancia = 0;
            for ( int i = indxAnt; i<indx; i++ ) distancia += gasol[i];
            s += "\n  - Gasolinera " + indx + " ( distancia desde la anterior = " + distancia + " )";
            indxAnt = indx;
        }
        
        distancia = 0;
        for ( int i = indxAnt; i<gasol.length; i++ ) distancia += gasol[i];
        s += "\n  - Distancia restante hasta el destino = " + distancia;
        
        System.out.println(s);
    }
        
    public static Reparto insercionManual() {
        double[] gasolineras = null;
        int n = 0;
        double autonomia = 0;
        
        while(true) {
          autonomia = obtenerDouble("Cauntos Km puede realizar sin repostar: ");
          if (!(autonomia <= 0)) break;
          System.out.println("El numero de kilometros tiene que ser positivo.");
        }
        
        while(true) {
          n = obtenerInt("Cuantos gasolineras hay en el trayecto: ");
          if (!(n < 0)) break;
          System.out.println("No puede haber un numero de gasolineras negativo.");
        }
        
        gasolineras = new double[n+1];
        for(int i=0; i<n; i++)
            gasolineras[i] = obtenerDouble("  - (Parada " + i + ") Distancia hasta la proxima gasolinera: ");
        
        gasolineras[n] = obtenerDouble("  - (Parada " + n + ") Distancia hasta el destino: ");
        
        return new Reparto(gasolineras, autonomia);
    }
    
    public static Reparto insercionAutomatica() {
        double[] gasolineras = null;
        int n = 0;
        double autonomia = 0;
        double nMin = 0, nMax = 0, nRango = 0;
        
        while(true) {
          autonomia = obtenerDouble("Cauntos Km puede realizar sin repostar: ");
          if (!(autonomia <= 0)) break;
          System.out.println("El numero de kilometros tiene que ser positivo.");
        }
        
        while(true) {
          n = obtenerInt("Cuantos gasolineras hay en el trayecto: ");
          if (!(n < 0)) break;
          System.out.println("No puede haber un numero de gasolineras negativo.");
        }
        
        gasolineras = new double[++n];
        
        do {
          nMin = obtenerDouble("Que valor tendra como minimo cada tramo: ");
          nMax = obtenerDouble("Que valor tendra como maximo cada tramo: ");
          if (nMin > nMax)
              System.out.println("El valor minimo no puede ser mayor al maximo.");
        } while (nMin > nMax);
        
        nRango = nMax - nMin;
        double distancia = 0;
        for(int i=0; i<n; i++) {
            distancia = nMin + Math.random()*nRango;
            gasolineras[i] = distancia;
            System.out.println("  - (Parada " + i + ") Distancia hasta la proximo punto: " + distancia);
        }
        
        return new Reparto(gasolineras, autonomia);
    }
    
    // Metodo el cual pregunta por un numero hasta que se introduce uno valido
    public static  int obtenerInt(String mensaje) {
            int numero = 0;
            boolean salir = false;
            Scanner teclado = new Scanner(System.in);
            String s = new String("");

            while (!salir) {
                    System.out.print(mensaje);
                    s = teclado.next();

                    // mientras que el programa no llegue a la variable salir se repetira este
                    try {
                            numero = Integer.parseInt(s);
                            salir = true;
                    }
                    catch (NumberFormatException e) {
                            System.out.print("(ERROR) El numero introducido no es valido.\n");
                    }
            }  

            return numero; // devolvemos el numero obtenido
    }
    

    // Metodo el cual pregunta por un numero hasta que se introduce uno valido
    public static double obtenerDouble(String mensaje) {
            double numero = 0;
            boolean salir = false;
            Scanner teclado = new Scanner(System.in);
            String s = new String("");

            while (!salir) {
                    System.out.print(mensaje);
                    s = teclado.next();

                    // mientras que el programa no llegue a la variable salir se repetira este
                    try {
                            numero = Double.parseDouble(s);
                            salir = true;
                    }
                    catch (NumberFormatException e) {
                            System.out.print("(ERROR) El numero introducido no es valido.\n");
                    }
            }  

            return numero; // devolvemos el numero obtenido
    }
}
